import {IonButtons, IonButton,
    IonContent, IonHeader, IonMenuButton, IonPage, IonTitle, IonToolbar} from '@ionic/react';
import React from 'react';
import {useParams} from 'react-router';
import './Page.css';
import Companies from '../features/companies/Companies';
import Staffs from '../features/staffs/Staffs';

const Page: React.FC = () => {

    const {name, id} = useParams<{ name: string; id: string}>();

    let content;
    if (name === 'companies' && !id){
        content = <Companies name={name}/>
    } else if (name === 'companies' && id) {
        content = <Staffs name={name} id={id}/>;
    }

    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonButtons slot="start">
                        <IonMenuButton/>
                    </IonButtons>
                    <IonTitle className="ion-text-capitalize">{name}</IonTitle>
                </IonToolbar>
            </IonHeader>

            <IonContent>
                <IonHeader collapse="condense">
                    <IonToolbar>
                        <IonTitle size="large"  className="ion-text-capitalize">{name}</IonTitle>
                    </IonToolbar>
                </IonHeader>
                {content}
            </IonContent>
        </IonPage>
    );
};

export default Page;
