import {StaffModel} from './staff.model';

export interface CompanyModel {
    id: number;
    name: string;
    address: string;
    state: string;
    city: string;
    zip: string;
    zips: string[];
    staffs: StaffModel[];

}