export interface UploadModel {
    duration: number;
    fileName: string;
    numberOfCompanyUploaded: number;
    numberOfDuplicate: number;
    numberOfProcessed: number;
    numberOfUploaded: number;
    totalNumber: number;
    started: number;
}