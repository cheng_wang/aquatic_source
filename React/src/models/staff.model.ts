export interface StaffModel {
    id: number;
    companyId: number;
    firstName: string;
    lastName: string;
    certNumber: string;

}