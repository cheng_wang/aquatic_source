import { combineReducers } from '@reduxjs/toolkit'

import companiesReducer from '../features/companies/CompaniesSlice';
import staffsReducer from '../features/staffs/StaffsSlice';

const rootReducer = combineReducers({
    companies: companiesReducer,
    staffs: staffsReducer
})

export type RootState = ReturnType<typeof rootReducer>

export default rootReducer