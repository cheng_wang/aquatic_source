import React, {useEffect, useRef, useState} from 'react';
import './Companies.css';
import {makeStyles} from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TablePagination from "@material-ui/core/TablePagination";
import Paper from '@material-ui/core/Paper';
import {useDispatch, useSelector} from 'react-redux';
import {fetchCompanies, setSelected} from './CompaniesSlice';
import {RootState} from '../../store/rootReducer';
import {cloudUploadOutline} from 'ionicons/icons';
import {
    IonButton,
    IonButtons,
    IonContent,
    IonFooter,
    IonIcon,
    IonItem,
    IonLabel,
    IonModal,
    IonProgressBar,
    IonText,
    IonTitle,
    IonToast,
    IonToolbar
} from '@ionic/react';
import {useHistory} from 'react-router';
import axios from 'axios';
import {UploadModel} from '../../models/upload.model';

interface Column {
    id: 'name' | 'address' | 'city' | 'state' | 'zip';
    label: string;
    minWidth?: number;
    align?: 'right';
    format?: (value: number) => string;
}

interface UploadState {
    file: File | '';
    fileName: string;
    uploading: boolean;
    upload?: UploadModel;
    showModal: boolean;
    error: string | null;
}


const columns: Column[] = [
    {id: 'name', label: 'Name', minWidth: 170},
    {id: 'address', label: 'Address', minWidth: 240},
    {
        id: 'city',
        label: 'City',
        minWidth: 170,
        align: 'right',
    },
    {
        id: 'state',
        label: 'State',
        minWidth: 40,
        align: 'right',
    },
    {
        id: 'zip',
        label: 'Zip',
        minWidth: 60,
        align: 'right',
    },
];


const useStyles = makeStyles({
    root: {
        width: '100%',
    },
    container: {
        maxHeight: 440,
    },
});


interface ContainerProps {
    name: string;
}

const Companies: React.FC<ContainerProps> = (props) => {

    /**
     * to demo how to use react-redux, thunk and reduxjs/toolkit
     */
    const dispatch = useDispatch();
    let {
        loading,
        companies,
        page,
        pageCount,
        error,
        totalCount
    } = useSelector((state: RootState) => state.companies);

    useEffect(() => {
        dispatch(fetchCompanies(page, pageCount))
    }, [dispatch])


    const classes = useStyles();

    const handleChangePage = (event: unknown, newPage: number) => {
        dispatch(fetchCompanies(newPage + 1, pageCount))
    };

    const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
        console.log(+event.target.value);
        dispatch(fetchCompanies(1, +event.target.value))
        // setRowsPerPage(+event.target.value);
        // setPage(0);
    };

    const showStaffs = (cId: number) => {
        var company = companies.find(c=> cId === c.id);
        dispatch(setSelected(company))
        history.push(`/page/companies/${cId}`);
    }

    let rowsContents =
        companies.map((row) => {
            return (
                <TableRow hover role="checkbox" tabIndex={-1} key={row.id} onClick={() => showStaffs(row['id'])}>
                    {columns.map((column) => {
                        const value = row[column.id];
                        return (
                            <TableCell key={column.id} align={column.align}>
                                {column.format && typeof value === 'number' ? column.format(value) : value}
                            </TableCell>
                        );
                    })}
                </TableRow>
            );
        });


    /**
     * to demo how to use local hook useState
     */
    const fileInput = useRef<HTMLInputElement>(null);

    const initialState: UploadState = {
        fileName: '', file: '',
        uploading: false, showModal: false,
        error: null
    };
    const [state, setState] = useState(initialState);
    const history = useHistory();


    const onFileSelected = (event: any) => {
        const file: File = event.target.files[0];
        if (file) {
            setState({...state, fileName: file.name, file: event.target.files[0]});
        }
    }

    const handleSubmit = (event: any) => {
        event.preventDefault();
        let form = new FormData();
        setState({...state, uploading: true})
        form.append('file', state.file)
        // @ts-ignore
        axios.post(`${window._env_.SERVICE_URL}/company`, form)
            .then(res => {

                // @ts-ignore
                fileInput.current.value = '';
                setState({...initialState, showModal: true, upload: res.data})
            }, reason => {

                // @ts-ignore
                fileInput.current.value = '';
                setState({...initialState, error: reason.toString()})
            });
    }

    const refreshCompanies = () => {
        setState({...state, showModal: false})
        dispatch(fetchCompanies(1, pageCount));
    };

    return (

        <Paper className={classes.root}>

            <IonToast
                isOpen={error != null}
                message={error || ''}
                position="top"
                color="warning"
                duration={2000}
            />

            <IonToast
                isOpen={state.error !== null}
                message={state.error || ''}
                position="top"
                color="warning"
                duration={2000}
            />

            <IonModal onDidDismiss={refreshCompanies}
                      isOpen={state.showModal}>

                <IonToolbar color="primary">
                    <IonTitle>Upload Successfully</IonTitle>
                </IonToolbar>
                <IonContent>
                    <IonItem>
                        <IonLabel color="primary">File name</IonLabel>
                        <IonText>{state.upload?.fileName}</IonText>
                    </IonItem>

                    <IonItem>
                        <IonLabel color="primary">Number of uploaded entries</IonLabel>
                        <IonText>{state.upload?.numberOfUploaded}</IonText>
                    </IonItem>

                    <IonItem>
                        <IonLabel color="primary">Number of entries are added</IonLabel>
                        <IonText>{state.upload?.numberOfProcessed}</IonText>
                    </IonItem>

                    <IonItem>
                        <IonLabel color="primary">Number of dupliate</IonLabel>
                        <IonText>{state.upload?.numberOfDuplicate}</IonText>
                    </IonItem>

                    <IonItem>
                        <IonLabel color="primary">Number of companies</IonLabel>
                        <IonText>{state.upload?.numberOfCompanyUploaded}</IonText>
                    </IonItem>
                    <IonItem>
                        <IonLabel color="primary">Processed time (mini-seconds)</IonLabel>
                        <IonText>{state.upload?.duration}</IonText>
                    </IonItem>
                </IonContent>
                <IonFooter className="ion-no-border">
                    <IonToolbar className="ion-text-center ion-padding-vertical">
                        <IonButton size="large" color="secondary"
                                   onClick={() => setState({...state, showModal: false})}>Close</IonButton>
                    </IonToolbar>
                </IonFooter>

            </IonModal>

            <form onSubmit={handleSubmit}>
                <IonToolbar>
                    <IonLabel slot="end">{state.fileName}</IonLabel>
                    <IonButtons slot="end">
                        <IonButton color="primary" className="fileUpload" id="btnFileUpload">Choose File
                            <input type="file" name="file" className="upload" id="uploadBtn" ref={fileInput}
                                   onChange={onFileSelected}/>
                        </IonButton>
                        <IonButton onClick={handleSubmit} color="primary"
                                   disabled={!state.file || state.uploading}> <IonIcon slot="icon-only"
                                                                                       icon={cloudUploadOutline}/>
                        </IonButton>
                    </IonButtons>
                </IonToolbar>
            </form>

            <IonProgressBar type={(state.uploading || loading) ? "indeterminate" : 'determinate'}></IonProgressBar>
            <TableContainer className={classes.container}>
                <Table stickyHeader aria-label="sticky table">
                    <TableHead>
                        <TableRow>
                            {columns.map((column) => (
                                <TableCell
                                    key={column.id}
                                    align={column.align}
                                    style={{minWidth: column.minWidth}}
                                >
                                    {column.label}
                                </TableCell>
                            ))}
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {rowsContents}
                    </TableBody>
                </Table>
            </TableContainer>
            <TablePagination
                rowsPerPageOptions={[10, 25, 100]}
                component="div"
                count={totalCount}
                rowsPerPage={pageCount}
                page={page - 1}
                onChangePage={handleChangePage}
                onChangeRowsPerPage={handleChangeRowsPerPage}
            />
        </Paper>
    );
};

export default Companies;
