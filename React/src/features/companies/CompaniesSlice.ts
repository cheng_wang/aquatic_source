import {createSlice, PayloadAction} from '@reduxjs/toolkit'

import {CompaniesResult, getCompanies, getCompany} from '../../api/companyApi'
import {AppThunk} from '../../store/store'
import {CompanyModel} from '../../models/company.model';

export interface CompaniesState {
    page: number
    totalCount: number
    pageCount: number
    companies: CompanyModel[]
    loading: boolean
    error: string | null
    selected: CompanyModel | null | undefined
}

const companiesInitialState: CompaniesState = {
    page: 1,
    totalCount: 0,
    pageCount: 25,
    companies: [],
    loading: false,
    error: null,
    selected: null
}

function startLoading(state: CompaniesState) {
    state.loading = true
}

function loadingFailed(state: CompaniesState, action: PayloadAction<string>) {
    state.loading = false
    state.error = action.payload
}

const companies = createSlice({
    name: 'companies',
    initialState: companiesInitialState,
    reducers: {
        getCompaniesStart: startLoading,
        getCompaniesSuccess(state, {payload}: PayloadAction<CompaniesResult>) {
            Object.assign(state, payload);
            state.loading = false;
        },
        getCompaniesFailure: loadingFailed,
        getCompanyStart: startLoading,
        getCompanySuccess(state, {payload}: PayloadAction<CompanyModel>) {
            state.selected = payload;
            state.loading = false;
        },
        getCompanyFailure: loadingFailed,
        setSelected(state, {payload}: PayloadAction<CompanyModel|undefined>) {
            state.selected = payload;
        }

    }
})

export const {
    getCompaniesStart,
    getCompaniesSuccess,
    getCompaniesFailure,
    getCompanyStart,
    getCompanySuccess,
    getCompanyFailure,
    setSelected
} = companies.actions

export default companies.reducer

export const fetchCompanies = (
    page?: number,
    pageCount?: number
): AppThunk => async dispatch => {
    try {
        dispatch(getCompaniesStart())
        const companies = await getCompanies(page, pageCount)
        dispatch(getCompaniesSuccess(companies))
    } catch (err) {
        dispatch(getCompaniesFailure(err.toString()))
    }
}

export const fetchCompany = (
    id: number
): AppThunk => async dispatch => {
    try {
        dispatch(getCompanyStart())
        const company = await getCompany(id)
        dispatch(getCompanySuccess(company))
    } catch (err) {
        dispatch(getCompanyFailure(err.toString()))
    }
}