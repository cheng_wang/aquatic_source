import {createSlice, PayloadAction} from '@reduxjs/toolkit'

import {StaffsResult, getStaffs} from '../../api/staffApi'
import {AppThunk} from '../../store/store'
import {StaffModel} from '../../models/staff.model';

export interface StaffsState {
    page: number
    totalCount: number
    pageCount: number
    staffs: StaffModel[]
    loading: boolean
    error: string | null
}

const staffsInitialState: StaffsState = {
    page: 1,
    totalCount: 0,
    pageCount: 25,
    staffs: [],
    loading: false,
    error: null
}

function startLoading(state: StaffsState) {
    console.log("start loading....")
    state.loading = true
}

function loadingFailed(state: StaffsState, action: PayloadAction<string>) {
    state.loading = false
    state.error = action.payload
}

const staffs = createSlice({
    name: 'staffs',
    initialState: staffsInitialState,
    reducers: {
        getStaffsStart: startLoading,
        getStaffsSuccess(state, {payload}: PayloadAction<StaffsResult>) {
            Object.assign(state, payload);
            state.loading = false;
        },
        getStaffsFailure: loadingFailed
    }
})

export const {
    getStaffsStart,
    getStaffsSuccess,
    getStaffsFailure
} = staffs.actions

export default staffs.reducer

export const fetchStaffs = (
    companyId: number,
    page?: number,
    pageCount?: number
): AppThunk => async dispatch => {
    try {
        dispatch(getStaffsStart())
        const staffs = await getStaffs(companyId, page, pageCount)
        dispatch(getStaffsSuccess(staffs))
    } catch (err) {
        dispatch(getStaffsFailure(err.toString()))
    }
}
