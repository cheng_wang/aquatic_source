import React, {useEffect} from 'react';
import './Staffs.css';
import {makeStyles} from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TablePagination from "@material-ui/core/TablePagination";
import Paper from '@material-ui/core/Paper';
import {useDispatch, useSelector} from 'react-redux';
import {fetchStaffs} from './StaffsSlice';
import {RootState} from '../../store/rootReducer';
import {IonButtons, IonLabel, IonProgressBar, IonToast, IonToolbar} from '@ionic/react';
import {fetchCompany} from '../companies/CompaniesSlice';

interface Column {
    id: 'firstName' | 'lastName' | 'certNumber';
    label: string;
    minWidth?: number;
    align?: 'right';
    format?: (value: number) => string;
}


const columns: Column[] = [
    {id: 'firstName', label: 'First Name', minWidth: 170},
    {id: 'lastName', label: 'Last Name', minWidth: 240},
    {
        id: 'certNumber',
        label: 'Certification Number',
        minWidth: 170,
        align: 'right',
    }
];


const useStyles = makeStyles({
    root: {
        width: '100%',
    },
    container: {
        maxHeight: 440,
    },
});


interface ContainerProps {
    name: string;
    id: string;
}

const Staffs: React.FC<ContainerProps> = (props) => {
    const {name, id} = props;

    /**
     * to demo how to use react-redux, thunk and reduxjs/toolkit
     */
    const dispatch = useDispatch();
    let {
        loading,
        staffs,
        page,
        pageCount,
        error,
        totalCount
    } = useSelector((state: RootState) => state.staffs);

    let {
        selected
    } = useSelector((state: RootState) => state.companies);


    useEffect(() => {
        if(!selected) {
            dispatch(fetchCompany(+id))
        }
        dispatch(fetchStaffs(+id, page, pageCount))
    }, [dispatch])


    const classes = useStyles();

    const handleChangePage = (event: unknown, newPage: number) => {
        dispatch(fetchStaffs(+id, newPage + 1, pageCount))
    };

    const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
        console.log(+event.target.value);
        dispatch(fetchStaffs(+id, 1, +event.target.value))
        // setRowsPerPage(+event.target.value);
        // setPage(0);
    };


    let rowsContents =
        staffs.map((row) => {
            return (
                <TableRow hover role="checkbox" tabIndex={-1} key={row.id}>
                    {columns.map((column) => {
                        const value = row[column.id];
                        return (
                            <TableCell key={column.id} align={column.align}>
                                {column.format && typeof value === 'number' ? column.format(value) : value}
                            </TableCell>
                        );
                    })}
                </TableRow>
            );
        });

    // const [selected, setState] = useState(companies.find(c => c.id === +id));


    return (

        <Paper className={classes.root}>

            <IonToast
                isOpen={error != null}
                message={error || ''}
                position="top"
                color="warning"
                duration={2000}
            />

            <IonToolbar>
                <IonLabel className="ion-margin" slot="start">{selected?.name}</IonLabel>
                <IonButtons slot="end">
                </IonButtons>
            </IonToolbar>


            <IonProgressBar type={loading ? "indeterminate" : 'determinate'}></IonProgressBar>
            <TableContainer className={classes.container}>
                <Table stickyHeader aria-label="sticky table">
                    <TableHead>
                        <TableRow>
                            {columns.map((column) => (
                                <TableCell
                                    key={column.id}
                                    align={column.align}
                                    style={{minWidth: column.minWidth}}
                                >
                                    {column.label}
                                </TableCell>
                            ))}
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {rowsContents}
                    </TableBody>
                </Table>
            </TableContainer>
            <TablePagination
                rowsPerPageOptions={[10, 25, 100]}
                component="div"
                count={totalCount}
                rowsPerPage={pageCount}
                page={page - 1}
                onChangePage={handleChangePage}
                onChangeRowsPerPage={handleChangeRowsPerPage}
            />
        </Paper>
    );
};

export default Staffs;
