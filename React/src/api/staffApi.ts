import axios from 'axios'
import {StaffModel} from '../models/staff.model';


export interface StaffsResult {

    pageCount: number,
    totalCount: number,
    page: number,
    staffs: StaffModel[]
}


export async function getStaffs(
    companyId: number,
    page = 1,
    pageCount = 25
): Promise<StaffsResult> {
    // @ts-ignore
    const url = `${window._env_.SERVICE_URL}/company/${companyId}/staffs?pageCount=${pageCount}&page=${page}`

    try {
        const response = await axios.get<StaffsResult>(url)
        return response.data;
    } catch (err) {
        throw err
    }
}