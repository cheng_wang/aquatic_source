import axios from 'axios'
import {CompanyModel} from '../models/company.model';


export interface CompaniesResult {
    pageCount: number,
    totalCount: number,
    page: number,
    companies: CompanyModel[]
}


export async function getCompanies(
    page = 1,
    pageCount = 25
): Promise<CompaniesResult> {
    // @ts-ignore
    const url = `${window._env_.SERVICE_URL}/companies?pageCount=${pageCount}&page=${page}`;

    try {
        const response = await axios.get<CompaniesResult>(url)
        return response.data;
    } catch (err) {
        throw err
    }
}

export async function getCompany(
    id:number
): Promise<CompanyModel> {
    // @ts-ignore
    const url = `${window._env_.SERVICE_URL}/company/${id}`;

    try {
        const response = await axios.get<CompanyModel>(url)
        return response.data;
    } catch (err) {
        throw err
    }
}