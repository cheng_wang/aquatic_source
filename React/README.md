#### How to run

- npm install
- npm start


#### How to run integration test
- npm run test:integration

#### How it works
- Components read data from a Redux store, and dispatch actions to the store to update data using @reduxjs/toolkit and redux-thunk

- Use hook and function component to build components

- Material UI

- Table Pagination

