const path = require('path')

describe('app', () => {
    beforeEach(async () => {
        await page.goto('http://localhost:3000')
    })

    it('should display a companies text', async () => {
        await expect(page).toMatch('companies')
    })

    it('should match a button with a "Choose File', async () => {
        await expect(page).toMatchElement('#btnFileUpload', { text: 'Choose File' })
    })

    it('should match a File Input with a "uploadBtn" class then fill it with a local file', async () => {
        await expect(page).toUploadFile(
            '#uploadBtn',
            path.join(__dirname, 'CompanyStaffList.csv'),
        )
    })
})