module.exports = {
    preset: 'jest-puppeteer',
    testRegex: './*\\.test\\.ts$',
    testTimeout: 30000

}

