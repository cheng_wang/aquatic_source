﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace aquatic_test
{
    public class CompanyContext : DbContext
    {
        // specify log level
        public static readonly ILoggerFactory DbLoggerFactory
            = LoggerFactory.Create(builder =>
            {
                builder.AddFilter((category, level) =>
category == DbLoggerCategory.Database.Command.Name
&& level == LogLevel.Information).AddConsole();
            });

        public DbSet<Company> Companies { get; set; }
        public DbSet<Staff> Staffs { get; set; }
        public DbSet<Zip> Zips { get; set; }
        public CompanyContext(DbContextOptions<CompanyContext> contextOptions): base(contextOptions)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Company>().HasIndex(c => c.CompanyUuid).
                IsUnique();

            modelBuilder.Entity<Staff>().HasIndex(c => c.StaffUuid).
                IsUnique();
        }

        //protected override void OnConfiguring(DbContextOptionsBuilder options)
        //    => options.UseSqlite("Data Source=company.db").UseLoggerFactory(MyLoggerFactory);

    }

    public class Company
    {
        [Key]
        public int CompanyId { get; set; }

        public string CompanyUuid { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Address { get; set; }

        [Required]
        public string City { get; set; }

        [Required]
        public string State { get; set; }

        [Required]
        public string Zip { get; set; }

        [InverseProperty("Company")]
        public List<Staff> Staffs { get; } = new List<Staff>();

        [InverseProperty("Company")]
        public List<Zip> Zips { get; } = new List<Zip>();

    }

    public class Staff
    {
        [Key]
        public int StaffId { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        public string CertificationNumber { get; set; }

        [Required]
        public string StaffUuid { get; set; }

        [Required]
        public int CompanyId { get; set; }

        public Company Company { get; set; }
    }

    public class Zip
    {
        [Key]
        public int ZipId { get; set; }
        public int Code { get; set; }

        public int CompanyId { get; set; }
        public Company Company { get; set; }
    }


}
