﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using aquatic_test.Interfaces;
using aquatic_test.Models;
using aquatic_test.Models.Dtos;
using FileHelpers;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;





namespace aquatic_test.Services
{
    /// <summary>
    /// implements services which are defined in the ICompanyService
    /// </summary>
    public class CompanyService : ICompanyService
    {
        private readonly CompanyContext _companyContext;
        private readonly ILogger<CompanyService> _logger;


        /// <summary>
        /// creates a instance of Company from the instance of StaffCsv
        /// </summary>
        /// <param name="staffCsv"></param>
        /// <returns>instance of Company</returns>
        public Company NewCompany(StaffCsv staffCsv)
        {
            return new Company()
            {
                Name = staffCsv.CompanyName,
                City = staffCsv.CompanyCity,
                State = staffCsv.CompanyState,
                Address = staffCsv.CompanyAddress,
                CompanyUuid = staffCsv.CompanyUuid(),
                Zip = staffCsv.CompanyZip


            };

        }

        /// <summary>
        /// checks if staff is in the staff list or not,
        /// if not, then creates and returns a instance of Staff,
        /// otherwise returns null
        /// </summary>
        /// <param name="staffCsv">instacne of StaffCsv</param>
        /// <param name="company">instacne of Company</param>
        /// <returns>Instance of Staff or null</returns>
        public Staff CreateStaffIfNotExists(StaffCsv staffCsv, Company company)
        {
            if (!company.Staffs.Exists((obj) => obj.StaffUuid.Equals(staffCsv.StaffUuid())))
            {

                return new Staff()
                {
                    FirstName = staffCsv.StaffFirstName,
                    LastName = staffCsv.StaffLastName,
                    Company = company,
                    CertificationNumber = staffCsv.StaffCertificationNumber,
                    StaffUuid = staffCsv.StaffUuid()
                };
            }

            return null;

        }


        public Company FindCompany(string uuid)
        {
            return _companyContext.Companies.Include(c => c.Staffs).Include(c => c.Zips).
                SingleOrDefault<Company>(c => c.CompanyUuid == uuid);
        }


        async public Task<IEnumerable<CompanyDto>> GetCompanies()
        {
            var companies = await this._companyContext.Companies.
                Include(c => c.Staffs).Include(c => c.Zips).ToListAsync<Company>();

            return from c in companies select new CompanyDto(c);

        }


        async public Task<CompanyResultDto> GetCompaniesByPage(int page, int pageCount)

        {
            //var companies = await this._companyContext.Companies.
            //    Include(c => c.Staffs).Include(c => c.Zips).ToListAsync<Company>();

            var companies =  await PaginatedList<Company>.CreateAsync(this._companyContext.Companies.
                Include(c => c.Staffs).Include(c => c.Zips).AsNoTracking(), page, pageCount);

            var companyDtos = from c in companies select new CompanyDto(c);
            return new CompanyResultDto()
            {
                companies = companyDtos,
                page = page,
                pageCount = pageCount,
                totalCount = this._companyContext.Companies.Count()
            };

        }


        async public Task<StaffResultDto> GetStaffsByPage(int companyId, int page, int pageCount)

        {

            var staffs = await PaginatedList<Staff>.CreateAsync(this._companyContext.Staffs.
                Where((Staff arg) => arg.CompanyId == companyId).AsNoTracking(), page, pageCount);

            var staffDtos = from c in staffs select new StaffDto(c);
            return new StaffResultDto()
            {
                staffs = staffDtos,
                page = page,
                pageCount = pageCount,
                totalCount = this._companyContext.Staffs.Where(arc => arc.CompanyId == companyId).Count()
            };

        }



        async public Task<IEnumerable<StaffDto>> GetStaffs()
        {
            var staffs = await this._companyContext.Staffs.ToListAsync<Staff>();

            return from s in staffs select new StaffDto(s);
        }

        async public Task<IEnumerable<StaffDto>> GetStaffsByCompanyId(int companyId)
        {
            var staffs = await this._companyContext.Staffs.
                Where((Staff arg) => arg.CompanyId == companyId).ToListAsync<Staff>();
            return from s in staffs select new StaffDto(s);
        }


        /// <summary>
        /// to preserve data integrity, add zip code to the zip code list.
        /// </summary>
        /// <param name="zipCode"></param>
        /// <param name="company"></param>
        public void UpdateZipCode(int zipCode, Company company)
        {
            if (!company.Zips.Exists(a => a.Code == zipCode))
            {
                company.Zips.Add(new Zip()
                {
                    Code = zipCode
                });
            }
        }

        /// <summary>
        /// processes CSV file which contains a list of denormalized staff data
        /// company unique id is the hash code of company name, address, city, state
        /// staff unique id is the hash code of company uuid, first name, last name
        /// and cerfitication number
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="stream"></param>
        /// <returns>the instance of UploadResponse</returns>
        async public Task<UploadResponse> ProcessCsv(string fileName, Stream stream)
        {
            DateTime started = DateTime.UtcNow;
            // prepare a list of raw data of staff from the stream of CSV file
            List<StaffCsv> staffs = new List<StaffCsv>();

            var engine = new FileHelperEngine<StaffCsv>();
            staffs = engine.ReadStreamAsList(new StreamReader(stream), Int32.MaxValue);

            this._logger.LogInformation("Total number of CSV:{count}", staffs.Count);

            // a list to store duplicate csv entries
            List<StaffCsv> duplicates = new List<StaffCsv>();

            Dictionary<String, Company> companies = new Dictionary<string, Company>();

            foreach (StaffCsv staffCsv in staffs)
            {
                //create a new instance of Company
                Company newCompany = NewCompany(staffCsv);

                Company company =
                    companies.ContainsKey(newCompany.CompanyUuid) ? companies[newCompany.CompanyUuid] :
                    this.FindCompany(staffCsv.CompanyUuid());

                // if the company does not exist in the database or in the company's dictionary,
                // then add the new created company into the company's dictionary and
                // into DB context to be persisted.
                if (company == null)
                {
                    company = newCompany;
                    this._companyContext.Companies.Add(newCompany);
                }

                companies[company.CompanyUuid] = company;


                // find staff
                Staff staff = CreateStaffIfNotExists(staffCsv, company);

                // add the staff into the staff list if the staff is not in the staff list
                // otherwise add the csv entry into the duplicate list
                if (staff != null)
                {
                    company.Staffs.Add(staff);
                }
                else
                {
                    duplicates.Add(staffCsv);
                }

                // TODO might not require to preserve a list of zip code since
                // zip code should be unique to the same address
                UpdateZipCode(Convert.ToInt32(staffCsv.CompanyZip), company);

            }


            await this._companyContext.SaveChangesAsync();

            int duration = Convert.ToInt32(DateTime.UtcNow.Subtract(started).TotalMilliseconds);

            this._logger.LogInformation("Total number of CSV:{count}", staffs.Count);

            return new UploadResponse()
            {
                FileName = fileName,
                TotalNumber = this._companyContext.Staffs.Count(),
                NumberOfDuplicate = duplicates.Count,
                NumberOfUploaded = staffs.Count,
                NumberOfProcessed = staffs.Count - duplicates.Count,
                Duplicates = duplicates,
                Started = started,
                Duration = duration,
                NumberOfCompanyUploaded = companies.Count
            };
        }

        /// <summary>
        /// constructor, logger and company DB context (Depedency Injection)
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="companyContext"></param>
        public CompanyService(ILogger<CompanyService> logger,
            CompanyContext companyContext)
        {
            this._logger = logger;
            this._companyContext = companyContext;

        }


    }
}
