﻿using System.Threading.Tasks;
using aquatic_test.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace aquatic_test.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CompanyController : ControllerBase
    {

        private readonly ILogger<CompanyController> _logger;
        private readonly ICompanyService _companyService;

        public CompanyController(ILogger<CompanyController> logger,
            ICompanyService companyService)
        {
            _logger = logger;
            _companyService = companyService;
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        async public Task<IActionResult> Upload(IFormFile file)
        {
            var stream = file.OpenReadStream();
            if (stream.Length == 0)
            {
                return BadRequest();
            }

            var response = await this._companyService.ProcessCsv(file.FileName, file.OpenReadStream());
            return Ok(response);

        }

        [Route("/staff")]
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        async public Task<IActionResult> GetStaffs()
        {
            var staffs = await this._companyService.GetStaffs();
            return Ok(staffs);
        }

        [HttpGet()]
        [ProducesResponseType(StatusCodes.Status200OK)]
        async public Task<IActionResult> GetCompanies()
        {
            var companies = await this._companyService.GetCompanies();
            return Ok(companies);
        }

        [Route("{id:int}/staffs")]
        [HttpGet()]
        [ProducesResponseType(StatusCodes.Status200OK)]
        async public Task<IActionResult> GetStaffsByCompanyId(int id, [FromQuery(Name = "page")] int page = 1, [FromQuery(Name = "pageCount")] int pageCount = 50)
        {
            var rs = await _companyService.GetStaffsByPage(id, page, pageCount);
            return Ok(rs);
        }

        [HttpGet("/companies")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        async public Task<IActionResult> GetCompaniesByPage([FromQuery(Name = "page")] int page = 1, [FromQuery(Name = "pageCount")] int pageCount = 50)
        {
            var companies = await this._companyService.GetCompaniesByPage(page, pageCount);
            return Ok(companies);
        }



    }
}
