### How it works

- Entity Framework Core 3.1
- Database entity model and DTO models
- Pagination
- LINQ 
- Dependency Injection