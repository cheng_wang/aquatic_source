﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using aquatic_test;
using aquatic_test.Controllers;
using aquatic_test.Models.Dtos;
using aquatic_test.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Xunit;

namespace tests
{
    public abstract class CompanyControllerTest
    {
        protected DbContextOptions<CompanyContext> ContextOptions { get; }
        public object Mock { get; private set; }

        #region Seeding
        protected CompanyControllerTest(DbContextOptions<CompanyContext> contextOptions)
        {
            ContextOptions = contextOptions;

            Seed();
        }


        private void Seed()
        {
            using (var context = new CompanyContext(ContextOptions))
            {
                context.Database.EnsureDeleted();
                context.Database.EnsureCreated();

                // prepare test seed data
            }
        }
        #endregion

        #region ProcessCsv
        [Fact]
        async public void ProcessCsvTest()
        {
            using (var context = new CompanyContext(ContextOptions))
            {
                var serviceProvider = new ServiceCollection()
                    .AddLogging()
                    .BuildServiceProvider();

                var factory = serviceProvider.GetService<ILoggerFactory>();

                var controllerLogger = factory.CreateLogger<CompanyController>();
                var serviceLogger = factory.CreateLogger<CompanyService>();
                var service = new CompanyService(serviceLogger, context);

                CompanyController companyController = new CompanyController(controllerLogger
                        , service);
                using (var stream = File.OpenRead("CompanyStaffList.csv"))
                {
                    var file = new FormFile(stream, 0, stream.Length, "file", Path.GetFileName(stream.Name))
                    {
                        Headers = new HeaderDictionary(),
                        ContentType = "text/csv"
                    };

                    var response = await companyController.Upload(file);
                    var okResult = response as OkObjectResult;
                    var uploadResponse = (UploadResponse)okResult.Value;

                    Assert.Equal(100, uploadResponse.NumberOfUploaded);

                    Assert.Equal(38, uploadResponse.NumberOfCompanyUploaded);

                    Assert.Equal(100, uploadResponse.NumberOfProcessed);



                }



                using (var stream = File.OpenRead("CompanyStaffList2.csv"))
                {
                    var file = new FormFile(stream, 0, stream.Length, "file", Path.GetFileName(stream.Name))
                    {
                        Headers = new HeaderDictionary(),
                        ContentType = "text/csv"
                    };

                    var response = await companyController.Upload(file);
                    var okResult = response as OkObjectResult;
                    var uploadResponse = (UploadResponse)okResult.Value;

                    Assert.Equal(3, uploadResponse.NumberOfUploaded);

                    Assert.Equal(2, uploadResponse.NumberOfCompanyUploaded);

                    Assert.Equal(1, uploadResponse.NumberOfProcessed);

                    Assert.Equal(2, uploadResponse.Duplicates.Count);
                }

                using (var stream = File.OpenRead("CompanyStaffList2.csv"))
                {
                    var file = new FormFile(stream, 0, stream.Length, "file", Path.GetFileName(stream.Name))
                    {
                        Headers = new HeaderDictionary(),
                        ContentType = "text/csv"
                    };

                    var response = await companyController.Upload(file);
                    var okResult = response as OkObjectResult;
                    var uploadResponse = (UploadResponse)okResult.Value;

                    Assert.Equal(3, uploadResponse.NumberOfUploaded);

                    Assert.Equal(2, uploadResponse.NumberOfCompanyUploaded);

                    Assert.Equal(3, uploadResponse.Duplicates.Count);


                    Assert.Equal(0, uploadResponse.NumberOfProcessed);
                }
            }
        }

        #endregion


        #region GetCompanies
        [Fact]
        async public void GetCompaniesTest()
        {
            using (var context = new CompanyContext(ContextOptions))
            {
                var serviceProvider = new ServiceCollection()
                    .AddLogging()
                    .BuildServiceProvider();

                var factory = serviceProvider.GetService<ILoggerFactory>();

                var controllerLogger = factory.CreateLogger<CompanyController>();
                var serviceLogger = factory.CreateLogger<CompanyService>();
                var service = new CompanyService(serviceLogger, context);
            

                CompanyController companyController = new CompanyController(controllerLogger
                        , service);

                using (var stream = File.OpenRead("CompanyStaffList.csv"))
                {
                    var file = new FormFile(stream, 0, stream.Length, "file", Path.GetFileName(stream.Name))
                    {
                        Headers = new HeaderDictionary(),
                        ContentType = "text/csv"
                    };

                    var response = await companyController.Upload(file);
                    var okResult = await companyController.GetCompanies() as OkObjectResult;
                    List<CompanyDto> companies = ((IEnumerable<CompanyDto>)okResult.Value).ToList();

                    Assert.Equal(38, companies.Count);

                    List<StaffDto> staffs = ((IEnumerable<StaffDto>) (await companyController.GetStaffs() as OkObjectResult).Value).ToList();
                    Assert.Equal(100, staffs.Count);


                    staffs = ((StaffResultDto)(await companyController.GetStaffsByCompanyId(companies[0].id) as OkObjectResult).Value).staffs.ToList();
                    Assert.Equal(companies[0].staffs.Count(), staffs.Count);

                }


            }

            #endregion

        }
    }
}
