﻿using System;
using aquatic_test;
using Microsoft.EntityFrameworkCore;

namespace tests
{
    public class SqliteCompanyControllerTest : CompanyControllerTest
    {
        public SqliteCompanyControllerTest(): base(
                new DbContextOptionsBuilder<CompanyContext>()
                    .UseSqlite("Filename=Test.db").UseLoggerFactory(CompanyContext.DbLoggerFactory)
                    .Options)
        {
        }
    }
}
