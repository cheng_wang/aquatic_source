﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using aquatic_test.Models.Dtos;

namespace aquatic_test.Interfaces
{
    public interface ICompanyService
    {
        Task<UploadResponse> ProcessCsv(string fileName, Stream stream);
        Task<CompanyDto> GetCompany(int id);
        Task<IEnumerable<StaffDto>> GetStaffs();
        Task<IEnumerable<CompanyDto>> GetCompanies();
        Task<CompanyResultDto> GetCompaniesByPage(int page, int pageCount);
        Task<StaffResultDto> GetStaffsByPage(int companyId, int page, int pageCount);
        Task<IEnumerable<StaffDto>> GetStaffsByCompanyId(int companyId);
    }
}
