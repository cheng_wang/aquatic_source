﻿using System.Linq;
using System.Collections.Generic;

namespace aquatic_test.Models.Dtos
{
    public class CompanyDto
    {
        public int id;
        public string name;
        public string address;
        public string state;
        public string city;
        public string zip;
        public IEnumerable<int> zips = new List<int>();
        public IEnumerable<StaffDto> staffs = new List<StaffDto>();
        public CompanyDto(Company company)
        {
            id = company.CompanyId;
            name = company.Name;
            address = company.Address;
            city = company.City;
            zip = company.Zip;
            state = company.State;
            staffs = from s in company.Staffs select new StaffDto(s);
            zips = from s in company.Zips select s.Code;
        }
    }
}
