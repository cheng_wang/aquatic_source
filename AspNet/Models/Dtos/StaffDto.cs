﻿using System;
namespace aquatic_test.Models.Dtos
{
    public class StaffDto
    {
        public int id;
        public int companyId;
        public string firstName;
        public string lastName;
        public string certNumber;
        public StaffDto(Staff staff)
        {
            id = staff.StaffId;
            companyId = staff.CompanyId;
            firstName = staff.FirstName;
            lastName = staff.LastName;
            certNumber = staff.CertificationNumber;
        }
    }
}
