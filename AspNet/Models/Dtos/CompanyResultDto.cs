﻿using System;
using System.Collections.Generic;

namespace aquatic_test.Models.Dtos
{
    public class CompanyResultDto
    {
        public int pageCount;
        public int totalCount;
        public int page;
        public IEnumerable<CompanyDto> companies;
    }
}
