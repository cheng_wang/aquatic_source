﻿using System;
using System.Collections.Generic;

namespace aquatic_test.Models.Dtos
{
    public class UploadResponse
    {
        public string FileName;

        public int NumberOfUploaded;

        public int NumberOfProcessed;

        public int NumberOfDuplicate;

        public int TotalNumber;

        public int NumberOfCompanyUploaded;

        public List<StaffCsv> Duplicates;

        // Datetime when it starts to process
        public DateTime Started;

        // Miniseconds
        public int Duration;
       
        public UploadResponse()
        {
        }
    }
}
