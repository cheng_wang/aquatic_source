﻿using System;
using aquatic_test.Utils;
using FileHelpers;

namespace aquatic_test.Models
{
    [DelimitedRecord(",")]
    [IgnoreFirst]
    public class StaffCsv
    {
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CompanyName;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CompanyAddress;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CompanyCity;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CompanyState;

       public string CompanyZip;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string StaffFirstName;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string StaffLastName;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string StaffCertificationNumber;

        // Dummy field to swallow the error
        // of having extra comma
        [FieldOptional]
        public string EmptyString;

        public String CompanyUuid()
        {
            return Md.MD5Hash(String.Format("{0}-{1}-{2}-{3}", CompanyName, CompanyAddress, CompanyCity, CompanyState));

        }

        public String StaffUuid()
        {
            return Md.MD5Hash(String.Format("{0}-{1}-{2}-{3}", CompanyUuid(), StaffFirstName, StaffLastName, StaffCertificationNumber));
        }

    }
}
