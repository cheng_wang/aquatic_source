### This project is an assignment from Aquatic Informatic



#### Run and verify application using docker. (docker images were published into docker hub)
 
1. Start Asp.Net app docker container 
   
        docker pull chengwang30/aquatic:latest
        docker run -p 8010:80 -d chengwang30/aquatic:latest
    
2. Start ReactApp docker container 
   
        docker pull chengwang30/aquatic-react:latest
        docker run -p 8020:80 -d chengwang30/aquatic-react:latest
   
3. Verify the application at http://localhost:8020

      1. Choose the file by clicking "CHOOSE FILE" button in the right upper corner.
      2. Upload the chosen file by clicking the button with "upload" icon in the right upper corner.
      3. If the file is uploaded successfully, then a modal window will show up with summary, otherwise a error message will be presented.
      4. Application shows a list of the imported companies in the data table.
      5. Click one of the companies in the data table, the employees in that company are shown, and the name of the choosen company name is shown at the upper left corner.
      6. Click the "Company" button at the left navigation bar, go back to the company list page
       
4. Check API definition from the link: http://localhost:8010
   
  
#### Build docker images from sources and run containers locally

1. Build Asp.Net app
    1. Open terminal window and cd to the root folder of Asp.Net solution (up folder of project).
     
            cd AspNet
        
    2. Build docker image 

            docker build -f aquatic-test/Dockerfile . -t aquatic:latest
       
    3. Run docker container
     
            docker run -p 8010:80 -d aquatic:latest
            
       
2. Build and Run ReactApp
    
    1. Open terminal window and cd to the root folder of ReactApp. 
    
            cd React
            
    2. Install dependencies.
    
            npm install
                 
    3. Run application
    
            npm run build
       
    4. Build docker image
    
            docker build . -t aquatic-react:latest

    5. Run docker container
    
            docker run -p 8020:80 -d aquatic-react:latest       
       
#### Test
- How to run test
  1. Asp.Net app
  
            dotnet test tests/tests.csproj
        
  2. ReactApp
  
          npm run test
  

#### Configuration

- Service url, public/assets/config/env-config.js

        window._env_ = {
          SERVICE_URL: "http://localhost:8010"
        }

